package com.example.notedemo.dbcontext;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.notedemo.model.Note;
import com.example.notedemo.utils.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CloudFirebaseContext {
    public interface CallBack {
        void onSuccess(List<Note> notes);

        void onFail(String message);
    }

    public interface AddNoteCallBack {
        void onSuccess(boolean b);

        void onFail(String message);
    }

    private FirebaseFirestore db;
    private static CloudFirebaseContext cloudFirebase;
    private List<Note> noteData;

    public CloudFirebaseContext() {
        db = FirebaseFirestore.getInstance();
        noteData = new ArrayList<>();
    }

    public static CloudFirebaseContext getInstance() {
        if (cloudFirebase == null) {
            cloudFirebase = new CloudFirebaseContext();
        }
        return cloudFirebase;
    }

    public void getAllNote(CallBack callBack) {
        db.collection("notes")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("haha", document.getId() + " => " + document.getData());
                                noteData.clear();
                                for (QueryDocumentSnapshot doc : task.getResult()) {
                                    String id = doc.getId();
                                    Map<String, Object> data = doc.getData();
                                    String title = (String) data.get(Constant.title);
                                    String description = (String) data.get(Constant.description);
                                    String createDate = (String) data.get(Constant.createDate);

                                    noteData.add(new Note(id, title, description, createDate));
                                }
                                callBack.onSuccess(noteData);
                            }
                        } else {
                            callBack.onFail(task.getException().toString());
                            Log.w("haha", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    public void addNote(Note newNote, AddNoteCallBack addNoteCallBack) {
        // Create a new user with a first and last name
        Map<String, Object> noteData = new HashMap<>();
        noteData.put(Constant.title, newNote.getTitle());
        noteData.put(Constant.description, newNote.getDescription());
        noteData.put(Constant.createDate, newNote.getCreateDate());

// Add a new document with a generated ID
        db.collection("notes")
                .add(noteData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        addNoteCallBack.onSuccess(true);
                        Log.d("haha", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        addNoteCallBack.onFail(e.getMessage());
                        Log.w("haha", "Error adding document", e);
                    }
                });
    }

    public void deleteNote(String id, AddNoteCallBack callBack) {
        db.collection("notes")
                .document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        callBack.onSuccess(true);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callBack.onFail(e.getMessage());
                    }
                });
    }

    public void updateNote(Note note, AddNoteCallBack callBack) {
        Map<String, Object> noteData = new HashMap<>();
        noteData.put(Constant.title, note.getTitle());
        noteData.put(Constant.description, note.getDescription());
        noteData.put(Constant.createDate, note.getCreateDate());

        db.collection("notes")
                .document(note.getId())
                .update(noteData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        callBack.onSuccess(true);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callBack.onFail(e.getMessage());
                    }
                });
    }
}
